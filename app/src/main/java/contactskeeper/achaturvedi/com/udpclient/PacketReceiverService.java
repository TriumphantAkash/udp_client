package contactskeeper.achaturvedi.com.udpclient;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by achaturvedi on 12/2/2015.
 */
public class PacketReceiverService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public PacketReceiverService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //receive UDP packets here
    }
}
