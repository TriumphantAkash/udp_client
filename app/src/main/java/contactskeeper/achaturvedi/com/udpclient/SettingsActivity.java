package contactskeeper.achaturvedi.com.udpclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SettingsActivity extends Activity {

    ConfigModel configData;
    Button toMainButton;
    EditText serverIP, serverPort, packetSize, numberOfPackets, packetInterval;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        serverIP = (EditText)findViewById(R.id.serverIP);
        serverPort = (EditText)findViewById(R.id.serverPort);
        packetSize = (EditText)findViewById(R.id.packetSize);
        numberOfPackets = (EditText)findViewById(R.id.numberOfPackets);
        packetInterval = (EditText)findViewById(R.id.packetInterval);
        toMainButton = (Button)findViewById(R.id.backToMain);

        configData = new ConfigModel();
        serverIP.setText(configData.getHostIP());
        serverPort.setText(configData.getHostPort()+"");
        Button.OnClickListener buttonHandler = new Button.OnClickListener() {
            public void onClick(View v) {
                backToMain();
            }
        };
        toMainButton.setOnClickListener(buttonHandler);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void backToMain() {

        if(!serverIP.getText().toString().equals("")) {
            configData.setHostIP(serverIP.getText().toString());
        }

        if(!serverPort.getText().toString().equals("")) {
            configData.setHostPort(Integer.parseInt(serverPort.getText().toString()));
        }

        if(!numberOfPackets.getText().toString().equals("")) {
            configData.setNumberOfPackets(Integer.parseInt(numberOfPackets.getText().toString()));
        }

        if(!packetSize.getText().toString().equals("")) {
            configData.setPacketSize(Integer.parseInt(packetSize.getText().toString()));
        }

        if(!packetInterval.getText().toString().equals("")) {
            configData.setPacketInterval(Integer.parseInt(packetInterval.getText().toString()));
        }

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("dataObject", configData);
        startActivity(intent);
    }
}
