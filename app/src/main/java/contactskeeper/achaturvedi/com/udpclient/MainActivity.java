package contactskeeper.achaturvedi.com.udpclient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity {
    Button b;
    //EditText serverIP, serverPort, packetSize, numberOfPackets, packetInterval;
    TextView status;
    ConfigModel configData;
    public Handler mHandler;
    File traceFile;
    String receivedMsg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b = (Button) findViewById(R.id.contactServer);
        status = (TextView) findViewById(R.id.status);
        configData = new ConfigModel();
        traceFile = new File(getApplicationContext().getExternalFilesDir(null), "UDP_streamer.txt");

        runOnUiThread(new Runnable() {
            @Override public void run() {
                if (Looper.myLooper() == null)
                {
                    Looper.prepare();
                }

                mHandler = new Handler() {
                    public void handleMessage(Message msg) {
                        // Act on the message
                        //we have the message now hahahahahahha
                        receivedMsg = msg.getData().getString("msg");
                        if(receivedMsg.equals("received all packets")) {
                            Toast.makeText(getApplicationContext(), "logs dumped to file", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.i("TEST_APP", "configuration packet received on the Main Thread as: \n" + receivedMsg);

                            status.append(receivedMsg);
                            //also write this data to a file
                            try {
                                // Creates a trace file in the primary external storage space of the
                                // current application.
                                // If the file does not exists, it is created.
                                // if (!traceFile.exists())
                                traceFile.createNewFile();
                                // Adds a line to the trace file
                                BufferedWriter writer = new BufferedWriter(new FileWriter(traceFile, true /*append*/));
                                writer.write(msg.getData().getString("msg"));
                                writer.close();
                                // Refresh the data so it can seen when the device is plugged in a
                                // computer. You may have to unplug and replug the device to see the
                                // latest changes. This is not necessary if the user should not modify
                                // the files.
                                MediaScannerConnection.scanFile(getApplicationContext(),
                                        new String[]{traceFile.toString()},
                                        null,
                                        null);

                            } catch (IOException e) {
                                Log.i("TEST_APP", "Unable to write to the TraceFile.txt file.");
                            }
                        }
                       // Log.i("TEST_APP", "configuration packet received on the Main Thread as: "+msg.getData().getString("msg2"));
                    }
                };
 //               Looper.loop();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        configData = (ConfigModel)getIntent().getSerializableExtra("dataObject");
        Log.i("alskjdlkasjd", "sample log");

        b.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (configData == null) {
                    Toast.makeText(getApplicationContext(), "Choose configuration first", Toast.LENGTH_SHORT).show();
                } else {
                    Second workerThread = new Second(configData, mHandler);
                    Thread t = new Thread(workerThread);
                    t.start();

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
        public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class Second implements Runnable {
        ConfigModel configModel;
        Handler referenceHandler;
        String msgToMainThread = "";
        Bundle bundle;
        Message msg ;

        Second(ConfigModel passedConfigModel, Handler mHandler) {
            configModel = passedConfigModel;
            referenceHandler = mHandler;

            Log.i("TEST_APP", "Second class handler called");
            //getApplicationContext();
        }
        public void run () {
            // TODO Auto-generated method stub
            Looper.prepare();
         //   Looper.loop();
            try {
                Log.i("TEST_APP", "1");
                //FileHandler fileHandler = new FileHandler(threadContext);
                long yourmilliseconds;
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
                Date resultdate;

                // String messageStr = "Hello from Android!";
                DatagramSocket socket = new DatagramSocket();
                Log.i("TEST_APP", "2");
                int server_port = configModel.getHostPort();
                Log.i("TEST_APP", "3");
                InetAddress serverIP = InetAddress.getByName(configModel.getHostIP());
                Log.i("TEST_APP", "4");

                //  int msg_length = messageStr.length();
                // byte[] message = messageStr.getBytes();

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                Log.i("TEST_APP", "5");

                ObjectOutput os = new ObjectOutputStream(outputStream);
                Log.i("TEST_APP", "6");

                os.writeObject(configModel);
                Log.i("TEST_APP", "7");

                byte[] data = outputStream.toByteArray();
                //  byte[] data = os.byte;

                DatagramPacket p = new DatagramPacket(data, data.length, serverIP,
                        server_port);
                Log.i("TEST_APP", "8");

                socket.send(p);
                Log.i("TEST_APP", "9");

                Log.i("TEST_APP", "configuration packet sent");

                ///////////////////////sending message to main thread///////////////////////////
//                Message msg = Message.obtain();
//
//                Bundle bundle = new Bundle();
//                bundle.putString("msg", "sampleMsg");
//                msg.setData(bundle);
//                referenceHandler.sendMessage(msg);

                //////////////////////////////////////////////////
                //also adding this data to file
               // fileHandler.writeNewData("Configuration packet to be stored in the file");

                //receive stream of UDP data from the server
                byte[] buffer = new byte[65536];
                DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);

                //client already knows how many packets are gonna get received, so run the loop accordingly
                int noOfPackets = configModel.getNumberOfPackets();

                for (int i = 0; i < noOfPackets; i++) {
                    socket.receive(receivePacket);
                    System.out.println("3");
                    yourmilliseconds = System.currentTimeMillis();
                    resultdate = new Date(yourmilliseconds);
                    /////////////////////////////////////////////////////////////////
                    Log.i("TEST_APP", "[" + sdf.format(resultdate) + "]" + "received #" + i + "packet of length : " + receivePacket.getLength());
                    msgToMainThread="[" + sdf.format(resultdate) + "]\n" + "received #" + i + "packet of length : " + receivePacket.getLength()+"\n";
                    msg = Message.obtain();
                    bundle = new Bundle();
                    bundle.putString("msg", msgToMainThread);
                    msg.setData(bundle);
                    referenceHandler.sendMessage(msg);
                    //////////////////////////////////////
//                    bundle.putString("msg2", "sampleMsghaha");
//                    msg.setData(bundle);
//                    referenceHandler.sendMessage(msg);

                    ///////////////////////////////////////

                }




                Log.i("TEST_APP", "received all packets");
                bundle.putString("msg", "received all packets");
                msg.setData(bundle);
                referenceHandler.sendMessage(msg);

                ////////////////////////////////////////////////////////////////
//            String sentence = new String(receivePacket.getData(), 0, receivePacket.getLength());
//
//            Log.i("TEST_APP", "Received this data from the server..... \n " + sentence);

            } catch (Exception e) {
                Log.i("UDPClient", "came in catch ");
            }
            Looper.loop();
        }

    }
}
