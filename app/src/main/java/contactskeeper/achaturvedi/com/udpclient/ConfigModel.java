package contactskeeper.achaturvedi.com.udpclient;

import java.io.Serializable;

/**
 * Created by achaturvedi on 11/20/2015.
 */
public class ConfigModel implements Serializable {
    private String hostIP;
    private int hostPort;
    private int packetSize;     //Bytes
    private int numberOfPackets;
    private int packetInterval; //sec
    private static final long serialVersionUID = 1L;

    ConfigModel() {
        //one of our public IP
        hostIP = "76.203.108.89";
        hostPort = 2225;
        packetSize = 800;
        numberOfPackets = 4;
        packetInterval = 3000;
    }

    public String getHostIP() {
        return hostIP;
    }

    public void setHostIP(String hostIP) {
        this.hostIP = hostIP;
    }

    public int getHostPort() {
        return hostPort;
    }

    public void setHostPort(int hostPort) {
        this.hostPort = hostPort;
    }

    public int getPacketSize() {
        return packetSize;
    }

    public void setPacketSize(int packetSize) {
        this.packetSize = packetSize;
    }

    public int getNumberOfPackets() {
        return numberOfPackets;
    }

    public void setNumberOfPackets(int numberOfPackets) {
        this.numberOfPackets = numberOfPackets;
    }

    public int getPacketInterval() {
        return packetInterval;
    }

    public void setPacketInterval(int packetInterval) {
        this.packetInterval = packetInterval;
    }
}
