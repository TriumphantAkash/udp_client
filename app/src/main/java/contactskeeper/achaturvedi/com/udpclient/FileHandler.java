package contactskeeper.achaturvedi.com.udpclient;

        import android.content.Context;
        import android.os.Environment;
        import android.util.Log;

        import java.io.BufferedReader;
        import java.io.File;
        import java.io.FileInputStream;
        import java.io.FileNotFoundException;
        import java.io.FileOutputStream;

        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.io.OutputStreamWriter;
        import java.io.PrintStream;
        import java.io.PrintWriter;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.Map;
        import java.util.regex.Matcher;
        import java.util.regex.Pattern;


/**
 * Created by achaturvedi on 12/15/2015.
 */
public class FileHandler {

    Context fileContext;//filecontext used to write the contents

    //passing the file context from the activity page
    FileHandler(Context fileContext){
        this.fileContext=fileContext;
    }

    public void writeNewData(String logData){
        clearContents();
        try {
            final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/" );
            File foo=new File(dir,"foo2.txt");//txtfilename

            PrintStream pr = new PrintStream(fileContext.openFileOutput(foo.getName(), Context.MODE_PRIVATE));

            pr.print(logData);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private void clearContents(){
        final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/" );
        File foo=new File(dir,"foo2.txt");
        try{
            FileOutputStream writer = fileContext.openFileOutput(foo.getName(),Context.MODE_PRIVATE);

            writer.write(("").getBytes());

            writer.close();
        }catch(FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}